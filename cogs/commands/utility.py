import requests
import re
import discord
import io
from discord.ext import commands


class Utility(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        name='be',
        aliases=['bigemoji'],
        help="Fetch a full size image of a given emoji",
        brief="Fetch emoji image"
    )
    async def be(self, ctx, text) -> None:
        emotes = re.findall(r'<\w?:\w*:\d*>', text)
        for emote in emotes:
            emoji_id = emote.split(':')[2].replace('>', '')
            r = requests.get("https://cdn.discordapp.com/emojis/" + emoji_id, allow_redirects=True)
            big_emoji = io.BytesIO(r.content)
            file = discord.File(big_emoji, emoji_id + ".gif")
            await ctx.send("", file=file)

    @commands.command(
        name='pp',
        aliases=['pfp'],
        help="Fetch a full size image of a given users avatar",
        brief="Fetch a users avatar"
    )
    async def pp(self, ctx, *, user: discord.User = None) -> None:
        if user is None:
            user = ctx.message.author
        await ctx.send(str(user.avatar_url_as(static_format='png')))


def setup(bot):
    bot.add_cog(Utility(bot))

import re

from discord import Embed, Reaction, User
from discord.ext import commands

from utils import get_guild_settings, save_guild_settings

EMOTE_OFFSET = 10

EMOJI_STATS_KEY = 'emoji_stats'
SPAM_CHANNEL_ID = 734198830031175730
NEXT_BUTTON_EMOJI = '⏭️'
PREV_BUTTON_EMOJI = '⏮️'


class EmojiStats(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.list_message = None
        self.offset = 0

    @commands.Cog.listener()
    async def on_message(self, message) -> None:
        if message.author == self.bot.user:
            return
        if message.channel.id == SPAM_CHANNEL_ID:
            return
        guild_settings = get_guild_settings(message.guild.id)
        if EMOJI_STATS_KEY not in guild_settings:
            guild_settings[EMOJI_STATS_KEY] = {}

        if any(str(emoji) in message.content for emoji in message.guild.emojis):
            emojis = re.findall(r'<a?:\w*:\d*>', message.content)
            for msg_emoji in emojis:
                if any(str(emoji) == msg_emoji for emoji in message.guild.emojis):
                    if msg_emoji not in guild_settings[EMOJI_STATS_KEY]:
                        guild_settings[EMOJI_STATS_KEY][msg_emoji] = 0
                    guild_settings[EMOJI_STATS_KEY][msg_emoji] += 1

            save_guild_settings(message.guild.id, guild_settings)

    @commands.Cog.listener()
    async def on_reaction_add(self, reaction, user) -> None:
        if user == self.bot.user:
            return
        if reaction.message.id == self.list_message.id:
            await self.edit_list_message(reaction, user)
            return
        if reaction.message.channel.id == SPAM_CHANNEL_ID:
            return
        str_emoji = str(reaction.emoji)
        guild_settings = get_guild_settings(reaction.message.guild.id)
        if 'emoji_stats' not in guild_settings:
            guild_settings[EMOJI_STATS_KEY] = {}

        for emoji in reaction.message.guild.emojis:
            if str_emoji == str(emoji):
                if emoji not in guild_settings[EMOJI_STATS_KEY]:
                    guild_settings[EMOJI_STATS_KEY][str_emoji] = 0
                guild_settings[EMOJI_STATS_KEY][str_emoji] += 1
                save_guild_settings(reaction.message.guild.id, guild_settings)
                break

    @commands.command(
        name='listemotes',
        help="List most used emotes on the server",
        brief="Emote statistics"
    )
    async def list_emotes(self, ctx) -> None:
        self.offset = 0
        emotes = await self.get_emote_list(ctx)

        self.list_message = await ctx.send(embed=Embed(color=0x2F3136, title='')
                                           .add_field(name='Emote statistics', value="\n".join(emotes)))
        await self.list_message.add_reaction(PREV_BUTTON_EMOJI)
        await self.list_message.add_reaction(NEXT_BUTTON_EMOJI)

    async def get_emote_list(self, ctx):
        guild_settings = get_guild_settings(ctx.guild.id)
        emojis = guild_settings[EMOJI_STATS_KEY]
        sorted_emojis = sorted(emojis, key=emojis.get, reverse=True)
        emotes = []
        number = 1 + self.offset
        for emoji in sorted_emojis[self.offset:self.offset + EMOTE_OFFSET]:
            emotes.append(f"{number}. {emoji}: {emojis[emoji]}")
            number += 1
        return emotes

    async def edit_list_message(self, reaction: Reaction, user: User):
        if reaction.emoji == NEXT_BUTTON_EMOJI:
            self.offset += EMOTE_OFFSET
        elif reaction.emoji == PREV_BUTTON_EMOJI:
            if self.offset != 0:
                self.offset -= EMOTE_OFFSET

        try:
            emotes = await self.get_emote_list(reaction.message)
            await self.list_message.edit(embed=Embed(color=0x2F3136, title='')
                                         .add_field(name='Emote statistics', value="\n".join(emotes)))
        except:
            self.offset -= EMOTE_OFFSET

        await reaction.remove(user)


def setup(bot) -> None:
    bot.add_cog(EmojiStats(bot))

import os
from os import path

import discord
from discord.ext import commands

extensions = []
for r, _, f in os.walk('cogs'):
    for name in f:
        if path.isfile(path.join(r, name)) and name.endswith('.py'):
            extensions.append(path.join(r, name).replace('/', '.')[0:-3])

bot = commands.Bot(command_prefix='>')


@bot.event
async def on_ready() -> None:
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')
    await bot.change_presence(activity=discord.Game('>help'))

if __name__ == "__main__":
    for extension in extensions:
        try:
            bot.load_extension(f'{extension}')
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            print(f'Failed to load extension {extension}\n{exc}')

    bot.run(os.environ.get('BOT_TOKEN'))

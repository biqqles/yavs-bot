FROM python:3-alpine

ARG COMMIT_HASH=0
ENV BOT_TOKEN="" \
    TENOR_API_KEY="" \
    COMMIT_HASH=$COMMIT_HASH

WORKDIR /usr/src/app

COPY requirements.txt ./

RUN apk add --no-cache --virtual .build-deps gcc musl-dev \
    && pip install --no-cache-dir -r requirements.txt \
    && apk del .build-deps gcc musl-dev

COPY . .

CMD [ "python", "-u", "./main.py" ]